# Jigsaw-Toxicity-Classification

### TODO
- [ ] Research
- [ ] Split data into folds
- [ ] Compare language models vs embeddings
- [ ] Implement baseline models
- [ ] Run and save results of baseline experiments on [neptune](https://ui.neptune.ml/lachonman/-/overview)

### Classification Data 
[Jigsaw Unintended Bias in Toxicity Classification Dataset](https://www.kaggle.com/c/jigsaw-unintended-bias-in-toxicity-classification/data)
#### Possible Language Model Datasets
- [ ] WikiText 103 [pretrained fastai](http://files.fast.ai/models/wt103/)

### Data Analysis
- [ ] Create data visualization
    - [ ] Comperate the size of vocab before and after normalization of the text (and effects of competing techniques)
    - [ ] Class distribution
    - [ ] Word distribution

### Data Agmentation Techniques
- By translation

#### NLP Preprocessing
- [ ] [spacy tokenizer](https://spacy.io/api/tokenizer)(Default Fastai tokenization)
- [ ] NLTK for lemmatization and stemming (Analize impact on models performance)

### Embedding
#### Words Embeddings
- [GloVe: Global Vectors for Word Representation](https://nlp.stanford.edu/projects/glove/)
- Fasttext
#### Documents Embeddings
- ...

### Models to check
#### Baselines
- [x] Random Forest
- [x] gradient boosted trees (xgboost and catboost)
- [x] [tpot AutoML](https://github.com/EpistasisLab/tpot)
- [ ] SVM + document embeddings
#### Out of the Box
- [ ] BERT
- [ ] ELMo
- [ ] The Transformer 
- [ ] FastAI transformer/transformerXL

### References and Resources:

#### Papers
- [Loss function for unbalanced class distribution](https://arxiv.org/abs/1708.02002)
- [Metric used in competition](https://arxiv.org/pdf/1903.04561.pdf)

#### Kernel links
- Toxicity-Classification
    - [basic ideas](https://www.kaggle.com/c/jigsaw-unintended-bias-in-toxicity-classification/discussion/87868#latest-519691)
    - [simple lstm 0.93010](https://www.kaggle.com/thousandvoices/simple-lstm)
    - [compact 0.93335](https://www.kaggle.com/gpreda/jigsaw-fast-compact-solution)
    - [Simple LSTM with Identity Parameters - FastAI 0.93524](https://www.kaggle.com/kunwar31/simple-lstm-with-identity-parameters-fastai)
    - [CNN in keras on folds 0.92341](https://www.kaggle.com/artgor/cnn-in-keras-on-folds)
- Imbalanced classes
    - [A CNN classifier and a Metric Learning model (1st)](https://www.kaggle.com/c/human-protein-atlas-image-classification/discussion/78109#latest-517369)
    - [pudae (3rd)](https://www.kaggle.com/c/human-protein-atlas-image-classification/discussion/77320#latest-469487)