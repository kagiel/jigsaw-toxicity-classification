# Ideas to improve model performance

 1. get corpus for language model that is similar to task dataset
   - [twitter 2018-07](https://archive.org/details/twitterstream?and[]=mediatype%3A%22data%22) [download link](https://archive.org/compress/archiveteam-twitter-stream-2018-07)
   - [reddit](https://files.pushshift.io/reddit/comments/)
 2. preprocess both copuses with by the same preprocessing (mb additional cleaning of html and stuff in lanuge corpora)
 3. check if adding old toxic when fittuning language model improves accuracy (if not mb it better generalizies)


- [stacking ensamble](https://mlwave.com/kaggle-ensembling-guide/)
- translate text into french and back to create synthehic samples (add noise in some other way like gaussian nosie to data)
- train langauge model on test data and old toxic comments data competition
- fix most common missing tokens
    - translate other langauge words inside text
    - find error that tokenizer might no handle correctly
- check if upsamling help (i'm almost sure using correct metric is better)
- implement or find the same metric for model as in competition description: [competition metric notebook](https://www.kaggle.com/dborkan/benchmark-kernel)
- implement or find loss function that improves performance of a model on unbalanced data
- create grid of hparams for model and check which are best (train a model to interpolate best or check other best hpar method picking like genetic or gradient desc)
- think about possible metrics of regularization (dropout, gradient clipping, l1/2, batch)
- [look for smart ideas form previous competitions](https://www.kaggle.com/c/jigsaw-unintended-bias-in-toxicity-classification/discussion/87648#latest-513431)
  - [tricks like pseudo-labelling](https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge/discussion/52557#latest-518528)
  - [super cool stuff like emoji to text](https://www.kaggle.com/c/jigsaw-unintended-bias-in-toxicity-classification/discussion/87245#latest-507767)  
- find a way to use extrac data provided inside the data (mb replace ethnic groups with tokens to remove bias)
- check how precision_recall_curve changes for models and plot it
- find missing tokens by loading vocab into word embedding matrixes and checking which are the most common missing tokens. Do that before and after spacy tokenization to make sure we only fix problems that are still occuring after using spacy
- maximize AUC instead of classification 
- replace words that can are distinctive for given ethnicity with tokens <enthnicity> so that model cant generalize to unwanted bias


[Benjamin Minixhofer kernel](https://www.kaggle.com/bminixhofer/simple-lstm-pytorch-version#Ways-to-improve-this-kernel)
- Add a contraction mapping. E. g. mapping "is'nt" to "is not" can help the network because "not" is explicitly mentioned. They were very popular in the recent quora competition, see for example this kernel.
- Try to reduce the number of words that are not found in the embeddings. At the moment, around 170k words are not found. We can take some steps to decrease this amount, for example trying to find a vector for a processed (capitalized, stemmed, ...) version of the word when the vector for the regular word can not be found. See the 3rd place solution of the quora competition for an excellent implementation of this.
- Try cyclic learning rate (CLR). I have found CLR to almost always improve my network recently compared to the default parameters for Adam. In this case, we are already using a learning rate scheduler, so this might not be the case. But it is still worth to try it out. See for example my my other PyTorch kernel for an implementation of CLR in PyTorch.
- Use sequence bucketing to train faster and fit more networks into the two hours. The winning team of the quora competition successfully used sequence bucketing to drastically reduce the time it took to train RNNs. An excerpt from their solution summary:

- We aimed at combining as many models as possible. To do this, we needed to improve runtime and the most important thing to achieve this was the following. We do not pad sequences to the same length based on the whole data, but just on a batch level. That means we conduct padding and truncation on the data generator level for each batch separately, so that length of the sentences in a batch can vary in size. Additionally, we further improved this by not truncating based on the length of the longest sequence in the batch, but based on the 95% percentile of lengths within the sequence. This improved runtime heavily and kept accuracy quite robust on single model level, and improved it by being able to average more models.

- Try a (weighted) average of embeddings instead of concatenating them. A 600d vector for each word is a lot, it might work better to average them instead. See this paper for why this even works.
- Limit the maximum number of words used to train the NN. At the moment, there is no limit set to the maximum number of words in the tokenizer, so we use every word that occurs in the training data, even if it is only mentioned once. This could lead to overfitting so it might be better to limit the maximum number of words to e. g. 100k.

