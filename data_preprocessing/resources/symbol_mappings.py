special_punc_mappings = {"—": "-", "–": "-", "_": "-", '”': '"', "″": '"', '“': '"', '•': '.', '−': '-',
                         "’": "'", "‘": "'", "´": "'", "`": "'", '\u200b': ' ', '\xa0': ' ','،':'','„':'',
                         '…': ' ... ', '\ufeff': ''}

char_emoji_normalization_list_mapping = {
    'love': ['<3'],
    'and': ['&'],
    'winking face': [';)', '; )', '(;', '( ;'],
    'smiling face': [':)' , ': )', '(:', '( :'],
    'grinning face': [':D'],
}

