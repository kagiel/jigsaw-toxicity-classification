import pandas as pd
import pathlib

LM_FOLDER_DATA = pathlib.Path('./toxic')
RESULT_LM_DF_FILE_NAME = 'combined_langauge_model_data.csv'

data_df_list = [pd.read_csv(x) for x in LM_FOLDER_DATA.iterdir()]
data_df_list_text = [df['comment_text'] for df in data_df_list]
df_full_lm_text = pd.concat(data_df_list_text, axis=0)
df_full_lm_text.to_csv(LM_FOLDER_DATA/RESULT_LM_DF_FILE_NAME, index=False, columns=['comment_text'])
