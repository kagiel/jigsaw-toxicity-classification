import abc
import functools
import pickle
import re
from typing import List

import contractions
from bs4 import BeautifulSoup
from markdown import markdown

from src.ml.text_models.resources import symbol_lists, symbol_mappings, word_mappings


class StrNormalizaitonStep(abc.ABC):
    @abc.abstractmethod
    def normalize_string(self, string_: str) -> str:
        pass


class StrPreprocessor(abc.ABC):
    @abc.abstractmethod
    def normalize_string(self, string_: str) -> str:
        pass


class LowerCase(StrNormalizaitonStep):
    def normalize_string(self, string_: str) -> str:
        return string_.lower()


class ReplaceCharactersUsingRegex(StrNormalizaitonStep):
    def __init__(self, old_characters_regex, new_characters, case_sensitive=True):
        self.old_characters_regex = old_characters_regex
        self.new_characters = new_characters
        self.flags = {'flags': re.I} if case_sensitive else {}

    def normalize_string(self, string_: str):
        return re.sub(self.old_characters_regex, self.new_characters, string_, **self.flags)


class ApplyRegexChain:
    def __init__(self, regex_operations: List[StrNormalizaitonStep]):
        self.regex_operations = regex_operations

    def normalize_string(self, string_):
        return functools.reduce(
            lambda string, regex_operation: regex_operation.normalize_string(string),
            self.regex_operations, string_)

    @classmethod
    def build_from_symbols_lists(cls, old_symbols: List[str], new_symbols: List[str]):
        return cls(
            regex_operations=[ReplaceCharactersUsingRegex(
                old_characters_regex=old_characters,
                new_characters=new_characters,
            ) for old_characters, new_characters in zip(old_symbols, new_symbols)]
        )

    @classmethod
    def build_many_into_one(cls, old_patterns: List[str], new_pattern: str):
        new_patterns = [new_pattern]*len(old_patterns)

        return cls.build_from_symbols_lists(
            old_symbols=old_patterns,
            new_symbols=new_patterns,
        )


class NormalizeSpecialSymbols(ApplyRegexChain):
    @classmethod
    def build_polish_alphabet(cls):
        return cls.build_from_symbols_lists(
            old_symbols=list('ćńóśźżąęł'),
            new_symbols=list('cnoszzael'),
        )

    @classmethod
    def build_markdown(cls):
        return cls.build_many_into_one(
            old_patterns=symbol_lists.extra_punct,
            new_pattern=' ',
        )

    @classmethod
    def build_quotation(cls):
        return cls.build_many_into_one(
            old_patterns=symbol_lists.strage_quotation_marks,
            new_pattern='"',
        )

    @classmethod
    def build_repeating_symbols(cls):
        return cls.build_many_into_one(
            old_patterns=[r'([ ])+', r'([!])+', r'([?])+'],
            new_pattern=r'\g<1>',
        )


class ReplaceReddit(ApplyRegexChain):
    @classmethod
    def build_removed_with_space(cls):
        return cls.build_from_symbols_lists(
            old_symbols=[r'[removed]', r'[deleted]'],
            new_symbols=[' ', ' '],
        )

    @classmethod
    def build_reddit_specific_with_spaces(cls):
        return cls.build_many_into_one(
            old_patterns=[r'r\/(\w+)\b', r'^/s'],
            new_pattern=' ',
        )


class ReplaceHTML(ApplyRegexChain):
    @classmethod
    def build_default(cls):
        return cls.build_from_symbols_lists(
            old_symbols=[r'&gt', r'&lt', '^>+', '&#32;'],
            new_symbols=['greater than', 'less_than', ' ', ' '],
        )


class ReplaceWhitespaces(ApplyRegexChain):
    @classmethod
    def build_remove_extra_spaces(cls):
        return cls.build_many_into_one(
            old_patterns=[' +'],
            new_pattern=' ',
        )

    @classmethod
    def build_remove_newlines(cls):
        return cls.build_many_into_one(
            old_patterns=[r'\n'],
            new_pattern=' ',
        )

    @classmethod
    def build_fix_not_decoded(cls):
        return cls.build_many_into_one(
            old_patterns=symbol_lists.spaces,
            new_pattern=' '
        )


class FixContractions(StrNormalizaitonStep):
    def normalize_string(self, string_: str) -> str:
        string_ = self.fix_contractions(string_)
        return self.fix_contraction_exception(string_)

    def fix_contractions(self, string_):
        return contractions.fix(string_)

    def fix_contraction_exception(self, string_):
        string_ = re.sub('do not you', 'do you not', string_, flags=re.I)
        string_ = re.sub('cannot', 'can not', string_, flags=re.I)
        return string_

    @classmethod
    def build_default(cls):
        return cls()

class ReplaceRepeatingCharactersWithSingleInstance(ReplaceCharactersUsingRegex):
    @classmethod
    def build_default(cls):
        return cls(
            old_characters_regex=r'([a-zA-Z])(\1+)',
            new_characters=r'\g<1>',
        )


class ReplaceNumbersWithSpace(ReplaceCharactersUsingRegex):
    @classmethod
    def build_default(cls):
        return cls(
            old_characters_regex=r'\d',
            new_characters=r' ',
        )


class ReplaceHyperlinksWithSpace(ReplaceCharactersUsingRegex):
    @classmethod
    def build_default(cls):
        return cls(
            old_characters_regex=r'https://\S+\b',
            new_characters=r' ',
        )


class ReplaceSpecialCharacters(ReplaceCharactersUsingRegex):
    @classmethod
    def build_with_spaces(cls):
        return cls(
            old_characters_regex=r'[^A-Za-z0-9,.?!ćńóśźżąęł]+',
            new_characters=r' ',
        )


class ReplaceEmojiWithSpace(ReplaceCharactersUsingRegex):
    @classmethod
    def build_default(cls):
        emoji_pattern =    ("["
                            u"\U0001F600-\U0001F64F"
                            u"\U0001F300-\U0001F5FF"
                            u"\U0001F680-\U0001F6FF"
                            u"\U0001F1E0-\U0001F1FF"
                            u"\U00002702-\U000027B0"
                            u"\U000024C2-\U0001F251"
                            u"\U0001f926-\U0001f937"
                            u'\U00010000-\U0010ffff'
                            u"\u200d"
                            u"\u2640-\u2642"
                            u"\u2600-\u2B55"
                            u"\u23cf"
                            u"\u23e9"
                            u"\u231a"
                            u"\u3030"
                            u"\ufe0f"
                            "]+")

        return cls(
            old_characters_regex=emoji_pattern,
            new_characters=' ',
        )


class MappingReplacer(StrNormalizaitonStep):
    def __init__(self, mapping):
        self.mapping = mapping

    def normalize_string(self, string_):
        for word in self.mapping:
            if word in string_:
                string_ = string_.replace(word, ' ' + self.mapping[word] + ' ')
        return string_

    @classmethod
    def build_correct_special_punctuation(cls):
        return cls(mapping=symbol_mappings.special_punc_mappings)

    @classmethod
    def build_correct_contraction(cls):
        return cls(mapping=word_mappings.contraction_mapping)

    @classmethod
    def build_correct_spelling(cls):
        return cls(mapping=word_mappings.mis_spell_mapping)

    @classmethod
    def build_emoji_to_text(cls):
        with open('./src/ml/text_models/resources/emoji_mapping_dict.pkl', 'rb') as mapping_file:
            emoji_mapping = pickle.load(mapping_file)
        return cls(mapping=emoji_mapping)


class FixMarkdown(StrNormalizaitonStep):
    def normalize_string(self, string_: str) -> str:
        html = markdown(string_)

        html = re.sub(r'<pre>(.*?)</pre>', ' ', html)
        html = re.sub(r'<code>(.*?)</code >', ' ', html)

        string_ = BeautifulSoup(html, "html.parser")
        string_ = ''.join(string_.findAll(text=True))
        return string_

    @classmethod
    def build_default(cls):
        return FixMarkdown()


class StrNormalizationPipeline(StrPreprocessor):
    def __init__(self, string_normalization_steps: List[StrNormalizaitonStep]):
        self.string_normalization_steps = string_normalization_steps

    def normalize_string(self, string_: str):
        for string_normalization_step in self.string_normalization_steps:
            string_ = string_normalization_step.normalize_string(string_)
        return string_

    @classmethod
    def build_english_reddit(cls):
        return cls(
            string_normalization_steps=[
                ReplaceWhitespaces.build_fix_not_decoded(),
                ReplaceWhitespaces.build_remove_extra_spaces(),
                NormalizeSpecialSymbols.build_quotation(),
                FixMarkdown.build_default(),
                NormalizeSpecialSymbols.build_markdown(),
                ReplaceHTML.build_default(),
                ReplaceHyperlinksWithSpace.build_default(),
                ReplaceWhitespaces.build_remove_newlines(),
                ReplaceReddit.build_removed_with_space(),
                ReplaceReddit.build_reddit_specific_with_spaces(),
                MappingReplacer.build_correct_special_punctuation(),
                MappingReplacer.build_emoji_to_text(),
                FixContractions.build_default(), # CONTRACTIONS
                NormalizeSpecialSymbols.build_repeating_symbols(),
                LowerCase(),
            ]
        )

    @classmethod
    def build_heavy_preprocessing_english_bullying(cls):
        return cls(
            string_normalization_steps=[
                ReplaceWhitespaces.build_remove_extra_spaces(),
                ReplaceHyperlinksWithSpace.build_default(),
                ReplaceNumbersWithSpace.build_default(),
                MappingReplacer.build_correct_spelling(),
                MappingReplacer.build_correct_special_punctuation(),
                MappingReplacer.build_correct_contraction(),
                ReplaceWhitespaces.build_fix_not_decoded(),
                ReplaceWhitespaces.build_remove_extra_spaces(),
            ]
        )

    @classmethod
    def build_light_preprocessing_english_bullying(cls):
        return cls(
            string_normalization_steps=[
                ReplaceWhitespaces.build_remove_extra_spaces(),
                ReplaceHyperlinksWithSpace.build_default(),
                ReplaceNumbersWithSpace.build_default(),
                MappingReplacer.build_correct_spelling(),
                MappingReplacer.build_correct_special_punctuation(),
                ReplaceWhitespaces.build_fix_not_decoded(),
                ReplaceWhitespaces.build_remove_extra_spaces(),
            ]
        )

    @classmethod
    def build_polish_bullying(cls):
        return cls(
            string_normalization_steps=[
                ReplaceEmojiWithSpace.build_default(),
                ReplaceCharactersUsingRegex(
                    old_characters_regex=r'\\n',
                    new_characters=' '),
                ReplaceHyperlinksWithSpace.build_default(),
                ReplaceNumbersWithSpace.build_default(),
                LowerCase(),
                ReplaceCharactersUsingRegex(
                    old_characters_regex=r'\bx d\b|\bxd\b|\bd\b',
                    new_characters=' '),
                ReplaceSpecialCharacters.build_with_spaces(),
                ReplaceCharactersUsingRegex(
                    old_characters_regex=r'([.,?!:])',
                    new_characters=r' \g<1> '),
                ReplaceWhitespaces.build_remove_extra_spaces(),
                ReplaceCharactersUsingRegex(
                    old_characters_regex=r'anonymized acount',
                    new_characters=r' '
                )
            ]
        )