import string_preprocess

str_pipe = StrNormalizationPipeline.build_english_reddit()
cores = multiprocessing.cpu_count()
partitions = cores

def text_clean_wrapper(df):
        df = df.apply(str_pipe.normalize_string)
        return df

def preprocess_parralel_column(df, func):
        col_split = np.array_split(df, num_partitions)
        pool = Pool(num_cores)
        col = pd.concat(pool.map(func, col_split))
        pool.close()
        pool.join()
        return col
