import numpy as np
import pandas as pd
from sklearn import model_selection

fold_count = 7
train_data_path = './train.csv'

data = pd.read_csv(train_data_path).reset_index(drop=True)

fold_spliter = model_selection.StratifiedKFold(
    n_splits=fold_count,
    shuffle=False,
)

for train_index, test_index in fold_spliter.split(data, np.zeros(data.shape[0])):
    train_data = data.iloc[train_index]
    test_data = data.iloc[test_index]
    train_data.to_csv('./train_fold_{}.csv'.format(fold_count), index=False)
    test_data.to_csv('./test_fold_{}.csv'.format(fold_count), index=False)
    fold_count -= 1
